## 环境配置
### 1. Python 3.5.2
1. 下载并安装python
	- 从python官网下载Python3.5.2 (3.7.7)，下载地址：`https://www.python.org/ftp/python/3.5.2/python-3.5.2-amd64.exe`

	- 运行exe安装包，勾选最下方的“Add Python 3.5 to PATH”，然后选择“Install Now”可完成安装

	- 打开cmd输入 `python -V` ，如果出现 `Python 3.5.2` 说明python已经装好了

2. 下载zeropc库
	- 在cmd中输入 `pip install zerorpc` 

### 2. Tensorflow 1.14.0

1. 在cmd中输入`pip install tensorflow==1.14.1`

2. 输入`python` ，`import tensorflow as tf` 和 `tf.__version__`，如果打印出了python的版本，证明安装成功


### 3. Tomcat  9.0.5
1. 从tomcat官网下载 Tomcat  9.0.5，解压并放到指定路径（推荐放到C盘的Program Files文件夹中），下载地址：
`https://archive.apache.org/dist/tomcat/tomcat-9/v9.0.5/bin/apache-tomcat-9.0.5-windows-x64.zip`

2. 配置Tomcat环境变量
	- 在 `此电脑 -> 属性 -> 更改设置 -> 高级 -> 环境变量`，在系统变量中添加：
		- TOMCAT_HOME变量，变量名为`TOMCAT_HOME` ，值为刚刚解压后的Tomcat的路径（如C:\Program Files\apache-tomcat-9.0.5）
		- CATALINA_HOME变量，变量名为`CATALINA_HOME` ，值和TOMCAT_HOME的值一样

	- 在系统变量中找到Path变量名，点编辑，然后新建，添加`%TOMCAT_HOME%\bin` 和 `%CATALINA_HOME%\lib` 这两个内容


	
## 启动服务
在配置完成上述环境后，按照如下步骤启动各项服务:

### 1.启动stanford nlp服务:
1. `cd D:\aiserver\sources\stanford-corenlp\3.8.0`
2. `java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer -port 9000 -timeout 1500`
终端显示出`StanfordCoreNLPServer listening at /0:0:0:0:0:0:0:0:9000`表示服务启动成功

### 2.启动深度学习模型服务:
1. `cd D:\aiserver\sources\ggnn`
2. `python AndroidModelServer.py`
终端显示出`on serving...`表示服务启动成功

### 3. 启动tomcat
1. 将D:\aiserver\sources中的CodeRecommendation.war文件拷贝到tomcat文件夹中的webapps文件夹目录下，例如C:\Program Files\apache-tomcat-9.0.5\webapps
2. cd到tomcat文件夹中bin文件夹目录，例如cd C:\Program Files\apache-tomcat-9.0.5\bin
3. 在cmd输入`startup`

### 4.
将插件中的 `http://bigcode.fudan.edu.cn/CodeRecommendationAPI` 修改为 `http://localhost:8080/CodeRecommendation`