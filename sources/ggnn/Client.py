import zerorpc
import sys

graphRepresent = sys.argv[1]
graphVocab = sys.argv[2]
variableName = sys.argv[3]

c = zerorpc.Client()
c.connect("tcp://127.0.0.1:4242")

#graphRepresent = "[[1,1,2],[1,1,3],[1,4,4],[2,1,5],[2,2,6],[3,1,7],[5,1,8],[5,2,6],[5,2,9],[6,1,10],[6,2,9],[8,1,11],[8,2,9],[8,2,12],[9,1,13],[11,1,6],[11,1,9],[11,1,12],[12,3,14],[14,1,15],[14,2,16],[15,3,16],[15,2,17],[16,1,17]]"
#graphVocab = "{1:'try',2:'java.io.BufferedReader.new(java.io.Reader)',3:'catch',4:'hole',5:'java.lang.String.Declaration',6:'java.io.BufferedReader.readLine()',7:'end',8:'java.lang.StringBuilder.new()',9:'java.lang.StringBuilder.append(java.lang.String)',10:'conditionEnd',11:'while',12:'java.lang.StringBuilder.toString()',13:'end',14:'java.lang.String.getBytes()',15:'java.security.MessageDigest.getInstance(java.lang.String)',16:'java.security.MessageDigest.update(byte[])',17:'java.security.MessageDigest.digest()'}"
#variableName = "path digest algorithm br str message builder origin datum md content"
ans = c.predict(graphRepresent,graphVocab,variableName)
print('startrecord')
for i in range(len(ans)):
	print (ans[i][0]," ",ans[i][1])